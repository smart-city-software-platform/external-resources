require 'csv'

CNES = []
CNES_USED = []

def loadCNES
	health_centre_csv_path = File.join(__dir__, "health_centres.csv")

	CSV.foreach(health_centre_csv_path, :headers => true) do |row|
		CNES[row[0].to_i] = row[1]
  	end
end

def printCn
	# CNES.each do |cn|
	# 	puts cn.keys
	# end
	puts CNES[206897423] == nil
end

def loadEstabelecimentos
	est_saude_csv_path = File.join(__dir__, "estab_saude.csv")
	puts "CNES,NOME_MAPA,LEITOS,TELEFONE,LONG,LAT\n"
	CSV.foreach(est_saude_csv_path, :headers => true) do |row|
		if CNES[row[5].to_i] != nil
			puts row[5] + "," + CNES[row[5].to_i] + "," + row[6] + "," + row[0] + "," + row[1] + "\n"
		end
	end
end

loadCNES()
loadEstabelecimentos()