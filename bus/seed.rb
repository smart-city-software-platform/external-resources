require_relative 'mongo_db'
require_relative 'olho_vivo'
require_relative 'platform_setup'
require_relative 'bus'
require_relative 'bus_stop'
require_relative 'bus_line'
require 'csv'
require 'colorize'
require 'thread/pool'

# This script saves GTFS files on MongoDB

if PlatformSetup.use_interscity?
  capabilities = [
    {
      name: "bus_monitoring",
      description: "Provides data related to a bus, such as its current location",
      type: "sensor"
    },
    {
      name: "bus_line_monitoring",
      description: "Provides data related to a bus line, such as the current number of buses operating on the line and their location",
      type: "sensor"
    },
    {
      name: "bus_line_metadata",
      description: "Contains static data related to bus lines, such as their frequencies and shape ",
      type: "sensor"
    },
    {
      name: "bus_stop_metadata",
      description: "Contains static data related to bus stop, such as its name and detailed description",
      type: "sensor"
    },
  ]

  capabilities.each do |capability|
    PlatformSetup.setup_capability(capability[:name], capability[:description], capability[:type])
  end
end

client = MongoDB.instance.client

collection = client[:bus_lines]

if collection.count == 0
  print "Reading frequencies file...".blue
  frequencies = {}
  CSV.foreach("gtfs/frequencies.csv", headers: true) do |row|
    id = row["trip_id"]
    if !frequencies.has_key?(id)
      frequencies[id] = []
    end
  
    frequencies[id] << {start_time: row["start_time"], end_time: row["end_time"], headway_secs: row["headway_secs"].to_i, headway_min: row["headway_secs"].to_i/60, number_of_trips: (3600.0/row["headway_secs"].to_i).round}
  end

  print "...Done!\n".green

  print "Reading shape file...".blue
  shapes = {}
  CSV.foreach("gtfs/shapes.csv", headers: true) do |row|
    id = row["shape_id"]
    if !shapes.has_key?(id)
      shapes[id] = []
    end
  
    shapes[id] << {lat: row["shape_pt_lat"].to_f, lon: row["shape_pt_lon"].to_f, sequence: row["shape_pt_sequence"].to_i, distance_traveled: row["shape_dist_traveled"].to_f}
  end

  print "...Done!\n".green

  puts "Inserting trips on database:".blue
  CSV.foreach("gtfs/trips.csv", headers: true) do |row|
    next if row["route_id"].include? "METR"
    trip = {route_id: row["route_id"], service_id: row["service_id"], trip_id: row["trip_id"], headsign: row["trip_headsign"], direction_id: row["direction_id"], shape_id: row["shape_id"], shape: shapes[row["shape_id"]], frequencies: frequencies[row["trip_id"]]}
    result = collection.insert_one(trip)
  end
  puts "#{collection.count} trips saved on database".green
else
  puts "There are trips on database already".yellow
end

api = OlhoVivo.new

puts "Collecting data from SPTRANS API".blue
collection.find(code: nil).each do |trip|
  found_trips = api.get_trip(trip[:route_id])
  if found_trips.nil?
    print 'F'.red

  end
  found_trips.each do |found_trip|
    if (trip[:direction_id].to_i == 0 && found_trip["sl"].to_i == 2) || (trip[:direction_id].to_i == 1 && found_trip["sl"].to_i == 1)
      new_fields = {code: found_trip["cl"].to_i, circular: found_trip["lc"], direction: found_trip["sl"]}
      collection.update_one({ trip_id: trip[:trip_id] }, '$set' => new_fields)
      print '.'.green
    else
      print '-'.yellow
      next
    end
  end
end

pool = Thread.pool(8)

if PlatformSetup.use_interscity?
  collection.find(uuid: nil).each do |bus_line|
    pool.process do 
      line = BusLine.new(bus_line)
      if PlatformSetup.create_resource(line)
        line = BusLine.find_by_code(line.code)
        data = {
          timestamp: Time.now
        }
        data = data.merge(line.metadata)
        PlatformSetup.post_data(line, "bus_line_metadata", data)
        print ".".green
      else
        print "F".red
      end
    end
  end
end
print "\n"
pool.shutdown

collection = client[:stops]

if collection.count == 0
  puts "Inserting bus stops on database:".blue
  CSV.foreach("gtfs/stops.csv", headers: true) do |row|
    bus_stop = {stop_id: row["stop_id"], stop_name: row["stop_name"], stop_desc: row["stop_desc"], lat: row["stop_lat"].to_f, lon: row["stop_lon"].to_f}
    result = collection.insert_one(bus_stop)
  end
  puts "#{collection.count} bus stops saved on database".green
else
  puts "There are bus stops on database already".yellow
end

pool = Thread.pool(8)
if PlatformSetup.use_interscity?
  collection.find(uuid: nil).each do |bus_stop|
    pool.process do 
      stop = BusStop.new(bus_stop)
      if PlatformSetup.create_resource(stop)
        stop = BusStop.find_by_id(stop.stop_id)
        data = {
          timestamp: Time.now
        }
        data = data.merge(stop.metadata)
        PlatformSetup.post_data(stop, "bus_stop_metadata", data)
        print ".".green
      else
        print "F".red
      end
    end
  end
end
print "\n"
pool.shutdown
