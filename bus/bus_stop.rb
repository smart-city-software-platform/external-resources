require 'date'
require_relative 'mongo_db'
require_relative 'olho_vivo'
require_relative 'bus'

class BusStop
  attr_accessor :stop_id, :stop_name, :stop_desc
  attr_accessor :uuid
  attr_accessor :lat, :lon, :description, :capabilities, :status

  def initialize(args={})
    args.each do |k,v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
    self.status = "active"
    self.description = "Parada de ônibus #{stop_name}"
    self.capabilities = ["bus_stop_metadata"]
  end

  def add_uuid(new_uuid)
    collection = MongoDB.instance.client[:stops]
    collection.update_one({stop_id: self.stop_id}, '$set' => {uuid: new_uuid})
  end
  
  def registered?
    !self.uuid.nil?
  end

  def data
    {
      lat: self.lat,
      lon: self.lon,
      description: self.description,
      capabilities: self.capabilities,
      status: self.status
    }
  end

  def metadata
    {
      name: self.stop_name,
      stop_id: self.stop_id,
      stop_description: self.stop_desc,
      type: "bus_stop",
    }
  end

  def self.collection(collection_name = :stops)
    MongoDB.instance.client[collection_name.to_sym]
  end

  def self.find_by_id(id)
    stop = BusStop.collection.find(stop_id: id).first
    stop.nil? ? nil : BusStop.new(stop)
  end
end
